const express = require('express');
const request = require('request');

const app = express();
const apiUrl = 'https://digitalapi.auspost.com.au/postcode/search.json';
const apiKey = '872608e3-4530-4c6a-a369-052accb03ca8';

app.get('/suburb', (req, res) => request({
  url: `${apiUrl}?q=${req.query.suburb}&state=${req.query.state}`,
  headers: {
    'auth-key': apiKey,
  },
}).pipe(res));

app.listen(process.env.PORT || 3001);
