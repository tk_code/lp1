import get from 'lodash/fp/get';

const localUrl = '/suburb';

/**
 * Connects to the local API proxy server to
 * get a list of localities and return the first
 * one whose Category is 'Delivery Area' and whose
 * field 'location' matches (case-insensitive) the given suburb name
 * @param  {string} suburbName Name of the suburb, from user input
 * @param  {string} state      ID of the state
 * @return {object}            A locality object, from Auspost API
 */

const findSuburb = suburbName => localityArr => Array.isArray(localityArr)
  ? localityArr
    .find(loc =>
      loc.category === 'Delivery Area' &&
      suburbName.trim().toLowerCase() === loc.location.trim().toLowerCase()
    )
  : null;

const getSuburbEntity = (suburbName, state) =>
  fetch(`${localUrl}?suburb=${suburbName}&state=${state}`)
    .then(response => response.json())
    .then(get('localities.locality'))
    .then(findSuburb(suburbName));

export { findSuburb, getSuburbEntity };
export default { findSuburb, getSuburbEntity };
