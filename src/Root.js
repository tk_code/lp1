import React, { Component } from 'react';
import { Provider } from 'react-redux';

import './Root.css';
import createStore from './createStore';
import AddressDetailsForm from './addressDetails/AddressDetailsForm';

/**
 * We need a redux store only to power redux-form (for now)
 * @type {object}
 */
const store = createStore();

/**
 * Root ReactElement for our app
 */
class Root extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="Root">
          <AddressDetailsForm />
        </div>
      </Provider>
    );
  }
}

export default Root;
