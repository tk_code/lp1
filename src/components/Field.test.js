/* global it, expect */
import React from 'react';
import { shallow } from 'enzyme';
import Field from './Field';

it('Renders without crashing', () => {
  const component = shallow(<Field />);
  expect(component.exists()).toEqual(true);
});
