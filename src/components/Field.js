import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Field as ReduxFormField } from 'redux-form';

const styles = {
  div: {
    margin: '4px 0',
    padding: '4px 0',
  },
  label: {
    display: 'block',
    clear: 'both',
  },
  error: {
    color: 'red',
    padding: '4px 0',
  },
};

/**
 * Internal ReactComponent used by Field
 * It handles validation messages
 */
const ValidatedField = ({
  realComponent,
  input,
  meta: { touched, error },
  ...props
}) => {
  const RealComponent = realComponent;
  return (<Fragment>
    <RealComponent {...props} {...input} />
    { touched && error && <div style={styles.error}>{error}</div>}
  </Fragment>);
};

ValidatedField.propTypes = {
  realComponent: PropTypes.oneOfType([PropTypes.func, PropTypes.string]).isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  input: PropTypes.object.isRequired,
  meta: PropTypes.shape({
    touched: PropTypes.bool.isRequired,
    error: PropTypes.string,
  }).isRequired,
};

/**
 * A wrapper for redux-form/Field that allows to add a label and handles error messages
 * @param  {string}           options.label     optional label
 * @param  {string|Component} options.component underlying component to use
 * @return {ReactElement}
 */
const Field = ({
  label,
  component,
  ...props
}) => (<div style={styles.div}>
  {/* eslint-disable jsx-a11y/label-has-for */}
  {label && <label style={styles.label}>{label}</label>}
  {/* eslint-enable */}
  <ReduxFormField {...props} component={ValidatedField} realComponent={component} />
</div>);

Field.propTypes = {
  label: PropTypes.string,
  component: PropTypes.oneOfType([PropTypes.func, PropTypes.string]).isRequired,
};

export default Field;
