import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import { compose, withState } from 'recompose';

const styles = {
  form: {
    border: '1px #ccc solid',
    background: '#eee',
    margin: '8px',
    padding: '8px',
    textAlign: 'center',
  },
  error: {
    background: 'rgba(255, 0, 0, 0.4)',
    padding: '8px',
    margin: '4px 0',
  },
  success: {
    background: 'rgba(0, 255, 0, 0.4)',
    padding: '8px',
    margin: '4px 0',
  },
  submit: {
    background: '#eee',
    minWidth: '10em',
  },
};

/**
 * Reusable Form component based on redux-form
 * It takes care of showing general error and success messages
 * Pass an onSubmit function, and children to populate it, with redux-form/Field included
 * @param  {function} props.handleSubmit  passed by redux-form
 * @param  {string} props.error           passed by redux-form
 * @param  {boolean} props.submitting     passed by redux-form
 * @param  {boolean} props.isSuccessful   handled by onSubmit
 * @param  {any} props.children           Populate the content of the Form
 * @return {ReactElement}
 */
const SimpleForm = ({
  handleSubmit,
  error,
  submitting,
  isSuccessful,
  children,
}) => (<form onSubmit={handleSubmit} style={styles.form}>
  {children}
  {error && <div style={styles.error}>{error}</div>}
  {isSuccessful &&
    <div style={styles.success}>The postcode, suburb and state entered are valid</div>}
  <div><button style={styles.submit} type="submit" disabled={submitting}>Submit</button></div>
</form>);

SimpleForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  error: PropTypes.string,
  isSuccessful: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  children: PropTypes.any,
};

const enhance = compose(
  withState('isSuccessful', 'setSuccess', false),
  reduxForm({
    form: 'simple-form',
  })
);

/**
 * The final Component
 * Only onSubmit property is required
 */
export default enhance(SimpleForm);
