/* global it, expect */
import React from 'react';
import { shallow } from 'enzyme';
import SimpleForm from './SimpleForm';

it('Renders without crashing', () => {
  const component = shallow(<SimpleForm />);
  expect(component.exists()).toEqual(true);
});
