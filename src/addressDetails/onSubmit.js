
import { SubmissionError } from 'redux-form';

import { getSuburbEntity } from '../services/PostApi';

/**
 * Processes the submit event in redux-form, throwing SubmissionError validation
 * or setting the successful state
 * @param  {string} values.postcode   postcode as input by user
 * @param  {string} values.suburb     suburb as input by user
 * @param  {string} values.state      state code as selected by user
 * @param  {function} options.setSuccess a function to handle the successful state of the form
 * @return {Promise}
 * @throws SubmissionError
 */

const testSuburbEntity = ({ postcode, suburb, state }) => (suburbEntity) => {
  if (!suburbEntity) {
    throw new SubmissionError({
      suburb: 'Suburb not found',
      _error: `The suburb ${suburb} does not exist in the state ${state}`,
    });
  } else if (postcode !== `${suburbEntity.postcode}`) {
    throw new SubmissionError({
      postcode: 'Postcode does not match the suburb',
      _error: `The postcode ${postcode} does not match the suburb ${suburb}`,
    });
  }
};

const onSubmit = (
  { postcode, suburb, state },
  _,
  { setSuccess }
) => {
  setSuccess(false);
  return getSuburbEntity(suburb, state)
    .catch((error) => {
      // eslint-disable-next-line no-console
      console.log('Error:', error);
      throw new SubmissionError({
        _error: 'An error happened validating your details. Please try again.',
      });
    })
    .then(testSuburbEntity({ postcode, suburb, state }))
    .then(() => setSuccess(true));
};

export { testSuburbEntity };
export default onSubmit;
