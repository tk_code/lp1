/* global it, expect, describe */
import { SubmissionError } from 'redux-form';

import { testSuburbEntity } from './onSubmit';

describe('function testSuburbEntity()', () => {
  const userInput = {
    state: 'NSW',
    postcode: '2000',
    suburb: 'Sydney',
  };

  const userInputWithWrongPostcode = {
    state: 'NSW',
    postcode: '2034',
    suburb: 'Sydney',
  };

  const sydneyLocation = ({
    postcode: 2000,
    state: 'NSW',
    location: 'SYDNEY',
  });

  const notFoundSuburbEntity = null;

  it('should not throw any error if the user input matches the found suburb', () => {
    expect(() => testSuburbEntity(userInput)(sydneyLocation))
      .not.toThrow(SubmissionError);
  });

  it('should throw a redux-form/SubmissionError if no suburb was found', () => {
    expect(() => testSuburbEntity(userInput)(notFoundSuburbEntity))
      .toThrow(SubmissionError);
    try {
      testSuburbEntity(userInput)(notFoundSuburbEntity);
    } catch (error) {
      expect(error.errors).toEqual({
        suburb: 'Suburb not found',
        _error: `The suburb ${userInput.suburb} does not exist in the state ${userInput.state}`,
      });
    }
  });

  it(
    'should throw a redux-form/SubmissionError if the postcode does not match the found location',
    () => {
      expect(() => testSuburbEntity(userInputWithWrongPostcode)(sydneyLocation))
        .toThrow(SubmissionError);
      try {
        testSuburbEntity(userInputWithWrongPostcode)(sydneyLocation);
      } catch (error) {
        expect(error.errors).toEqual({
          postcode: 'Postcode does not match the suburb',
          _error: `The postcode ${userInputWithWrongPostcode.postcode} does not match the suburb ${userInputWithWrongPostcode.suburb}`,
        });
      }
    }
  );
});
