import React from 'react';

import SimpleForm from '../components/SimpleForm';
import Field from '../components/Field';
import onSubmit from './onSubmit';

/**
 * redux-form validator for required fields
 * @param  {any} value
 * @return {string|undefined}
 */
const required = value => value ? undefined : 'Required';

/**
 * A redux-form based Form that asks for suburb, postcode and state
 * and delegates the back-end processing to onSubmit
 * @return {Element}
 */
const AddressDetailsForm = () => (<SimpleForm onSubmit={onSubmit}>
  <h2>Enter the details of your address</h2>
  <Field label="Suburb" name="suburb" type="text" component="input" validate={[required]} />
  <Field label="Postcode" name="postcode" component="input" type="text" validate={[required]} />
  <Field label="State" name="state" component="select" validate={[required]}>
    <option />
    <option value="ACT">Australian Capital Territory</option>
    <option value="JBT">Jervis Bay Territory</option>
    <option value="NSW">New South Wales</option>
    <option value="NT">Northen Territory</option>
    <option value="SA">South Australia</option>
    <option value="TAS">Tasmania</option>
    <option value="QLD">Queensland</option>
    <option value="VIC">Victoria</option>
    <option value="WA">Western Australia</option>
  </Field>
</SimpleForm>);

export default AddressDetailsForm;
