import { createStore, combineReducers } from 'redux';
import { reducer as form } from 'redux-form';

export default () => createStore(
  combineReducers({ form }),
  /* eslint-disable no-underscore-dangle */
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  /* eslint-enable */
);
