# LawPath Tech Test
by Gabriel Mérida, http://www.gabrielmerida.cl

## Task
> Create an online form using React that accepts a postcode, suburb and state. When the user submits the form, it should check the inputs with the Australia Post API to validate that it is a valid address.
> Tests to pass:
> a.        Check that the entered postcode matches the suburb. If not display an error message. For example "The postcode 2000 does not match the suburb Broadway".
> b.        Check that the entered suburb matches the state. If not display an error message. For example: "The suburb Ferntree Gully does not exist in the state Tasmania"
> c.        If the postcode, suburb and state match, then display a success message. For example: "The postcode, suburb and state entered are valid".
> How to Share the Code
> The project should be created as a Github or Bitbucket repo.  The readme of the repository should contain instructions on how to recreate and run the project locally.
> Key Objectives:·  
> ·  ability to develop an application using React
> ·  ability to interact with a REST API
> ·  ability to create an online form
> ·  competence with HTML5/CSS/Javascript
> ·  competence with git
> ·  demonstrate ability to write automated tests
> Online form, React accepts a postcode, suburb and state.
> When the user submits the form, it should check the inputs with the Australia Post API to validate that it is a valid address.
> Tests to pass:
> a. Check that the entered postcode matches the suburb. If not display an error message. For example "The postcode 2000 does not match the suburb Broadway".
> b. Check that the entered suburb matches the state. If not display an error message. For example: "The suburb Ferntree Gully does not exist in the state Tasmania"
> c. If the postcode, suburb and state match, then display a success message. For example: "The postcode, suburb and state entered are valid".
> Details of the API can be found at: https://developers.auspost.com.au/apis/pac/reference/postcode-search
> You can use the key: 872608e3-4530-4c6a-a369-052accb03ca8 on the https://digitalapi.auspost.com.au url

## Solution and decisions

- I have built a very basic form using React and redux-form, with a little help from lodash and recompose.
- The Auspost API has not worked with cors from a separate domain, so I have used a backend proxy in express to access it.
- The starting files are `serve.js` for the proxy and `src/index.js`. Start from the comments in index and follow the rest of the files in `src`.
- I have used JS styles for the form, althoug I do not have a final decision about the existential question of using CSS classes or CSS in JS.
- The folder structure assume that this project can grow, and so, we have re-usable <SimpleForm /> and <Field /> components and a specific <AddressDetailsForm />, which may seem overkill for this test, but for me is a better way to structure any solution.

## How to run

- Download the repository. Run:
    ```
    npm install;
    npm start;
    ```
- Visit http://localhost:3000/

- To test, run `npm run test`

## Fix

There was an error on my part understanding the API correctly. It's fixed now, and some tests were added for the functions that process the data from the API.